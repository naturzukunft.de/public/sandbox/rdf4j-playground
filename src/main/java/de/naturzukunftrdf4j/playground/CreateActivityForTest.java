package de.naturzukunftrdf4j.playground;

import static org.eclipse.rdf4j.model.util.Values.iri;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Namespace;
import org.eclipse.rdf4j.model.impl.SimpleNamespace;
import org.eclipse.rdf4j.model.util.ModelBuilder;
import org.eclipse.rdf4j.model.vocabulary.FOAF;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.model.vocabulary.RDFS;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.Rio;

public class CreateActivityForTest {
	
	public static final String NAMESPACE = "http://example.org/ap/";
	public static Namespace NS = new SimpleNamespace("ap", NAMESPACE );
	private static List<Namespace> namespaces = new ArrayList<>();
	
	public final static IRI NOTIFICATION = iri(NAMESPACE, "notification");
	
	static {
		namespaces.add(NS);
		namespaces.add(FOAF.NS);
		namespaces.add(RDFS.NS);
		namespaces.add(RDF.NS);
		namespaces.add(NS);
		namespaces.add(AS.NS);
	}

	public Model createActivity(IRI notificationIRI, String name, String givenNName, String mail, String comment) {
		IRI to = iri(NAMESPACE, "actors/" + "admin"); // <- TODO
		String userId = UUID.randomUUID().toString();
		IRI actor = iri(NAMESPACE, "actors/" + userId);
		Model model = createActivity(notificationIRI, actor, to, name, givenNName, mail, comment);
//		model.forEach(System.out::println);
		return model;
	}

	public Model createActivity(String name, String givenNName, String mail, String comment) {
		String userId = UUID.randomUUID().toString();
		IRI to = iri(NAMESPACE, "actors/" + "admin"); // <- TODO
		IRI actor = iri(NAMESPACE, "actors/" + userId);
		return createActivity(actor, to, name, givenNName, mail, comment);
	}
	
	public Model createActivity(IRI actor, IRI to, String name, String givenNName, String mail, String comment) {
		IRI notificationIRI = iri(NAMESPACE, NOTIFICATION.getLocalName() + "_" + UUID.randomUUID());
		return createActivity(notificationIRI, actor, to, name, givenNName, mail, comment);
	}
	
	public Model createActivity(IRI notificationIRI, IRI actor, IRI to, String name, String givenNName, String mail, String comment) {
		return createModelBuilder()
				.subject(notificationIRI)
					.add(RDF.TYPE, AS.Announce)
					.add(AS.actor, actor)
					.add(AS.summary, comment)
					.add(AS.object, actor)
					.add(AS.target, to)
					.add(AS.to, to)
				.subject(actor)
					.add(RDF.TYPE, AS.Person)
					.add(AS.name, name + " " + givenNName)
					.add(AS.id, actor)
					.add(FOAF.NAME, name)
					.add(FOAF.GIVEN_NAME, givenNName)
					.add(FOAF.MBOX, mail).build();
	}

	public ModelBuilder createModelBuilder() {
		ModelBuilder builder = new ModelBuilder();
		namespaces.forEach(builder::setNamespace);
		return builder;
	}
	
	public String toString(Model model) {
		StringWriter sw = new StringWriter();
		Rio.write(model, sw, RDFFormat.TURTLE);		
		return sw.toString();
	}
}
