package de.naturzukunftrdf4j.playground;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.Rio;

public class ActivityStreamsDefinition {

	public static void main(String[] args) throws Exception {

		String content = getAsSpec();
		System.out.println(content);
		Model model = Rio.parse(new StringReader(content), RDFFormat.TURTLE);
		model.getStatements(null, null, null).forEach(System.out::println);
	}

	private static String getAsSpec() throws MalformedURLException, IOException, ProtocolException {
		URL url = new URL(
				"https://raw.githubusercontent.com/w3c/activitystreams/wd-20160712/vocabulary/activitystreams2.owl");
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod("GET");
		con.setRequestProperty("Accept", "application/ld+json");

		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer contentBuffer = new StringBuffer();
		while ((inputLine = in.readLine()) != null) {
			contentBuffer.append(inputLine);
		}
		in.close();

		con.disconnect();

		String content = contentBuffer.toString();
		return content;
	}
}
