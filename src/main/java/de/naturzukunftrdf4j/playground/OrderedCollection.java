package de.naturzukunftrdf4j.playground;

import static org.eclipse.rdf4j.model.util.Values.bnode;
import static org.eclipse.rdf4j.model.util.Values.iri;

import java.io.IOException;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.model.impl.LinkedHashModel;
import org.eclipse.rdf4j.model.impl.TreeModelFactory;
import org.eclipse.rdf4j.model.util.RDFCollections;
import org.eclipse.rdf4j.query.TupleQuery;
import org.eclipse.rdf4j.query.TupleQueryResult;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.RepositoryResult;
import org.eclipse.rdf4j.repository.sail.SailRepository;
import org.eclipse.rdf4j.sail.memory.MemoryStore;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class OrderedCollection {
	
	public static final IRI JOHN = iri("http://example.org/john/");
	public static final IRI MARC = iri("http://example.org/marc/");
	private Repository repo = new SailRepository(new MemoryStore());
	private CreateActivityForTest createActivityForTest = new CreateActivityForTest();
	
	public static final void main(String args[]) throws IOException {
		OrderedCollection instance = new OrderedCollection();
		instance.run();
	}

	public void run() throws IOException {
		
		IRI activityIRI1 = createRandomActivityIRI();
		save(JOHN, createActivityForTest.createActivity(activityIRI1, "customer1", "mustermann", "cust1@mustermann.de", "Ich habe interesse"));

		IRI activityIRI2 = createRandomActivityIRI();
		save(JOHN, createActivityForTest.createActivity(activityIRI2, "customer2", "Carpenter", "cust2@carpenter.de", "I'm interessted"));

		List<IRI> johnsActivities = List.of(activityIRI1,activityIRI2);
		saveCollection(JOHN, johnsActivities);
		johnsActivities  = getOutbox(JOHN);
		johnsActivities.forEach(this::printActivity);
		
		List<IRI> marcsActivities = List.of(
				iri(getOutboxIri(MARC).stringValue(), UUID.randomUUID().toString()),
				iri(getOutboxIri(MARC).stringValue(), UUID.randomUUID().toString()),
				iri(getOutboxIri(MARC).stringValue(), UUID.randomUUID().toString()));
		
		saveCollection(MARC, marcsActivities);			
		marcsActivities = getOutbox(MARC);
		marcsActivities.forEach(System.out::println);
	}

	private void printActivity(IRI activityId) {
		try(RepositoryConnection con = repo.getConnection()) {
						
			// i know, this should be done with sparql, but i've to play with sparql before.
			RepositoryResult<Statement> result = con.getStatements(activityId, null, null);
			Model model = new TreeModelFactory().createEmptyModel();
			result.forEach(stmt -> model.add(stmt));
			Statement first = model.getStatements(activityId, AS.object, null).iterator().next();
			IRI objectIri = iri(first.getObject().stringValue());
			result = con.getStatements(objectIri, null, null);
			result.forEach(stmt -> model.add(stmt));
			
			System.out.println("-> ##### " + activityId + " ####");
			model.forEach(System.out::println);
			System.out.println("<- ##### ");
		}
	}
	
	private IRI createRandomActivityIRI() {
		return iri(getOutboxIri(JOHN).stringValue(), AS.Announce.getLocalName() + "_" + UUID.randomUUID());
	}

	private void save(IRI actor, Model activity) {
		try(RepositoryConnection con = repo.getConnection()){
			con.add(activity);
		}			
	}
	
	private void saveCollection(IRI actor, List<IRI> activities) {
		Resource head = bnode();
		Model inbox = RDFCollections.asRDF(activities, head, new LinkedHashModel());
		try(RepositoryConnection con = repo.getConnection()){
			con.add(inbox);
		}			
	}

	private IRI getOutboxIri(IRI actor) {
		return iri(actor.stringValue(), "outbox/");
	}
	
	private List<IRI> getOutbox(IRI actor) {
		try(RepositoryConnection con = repo.getConnection()) {
			TupleQuery query = con.prepareTupleQuery(getItemsQuery(getOutboxIri(actor)));
			TupleQueryResult queryResult = query.evaluate();
			return 
					queryResult.stream()
					.map(it-> iri(it.getValue("member").stringValue()))
					.collect(Collectors.toList());
		}
	}

	private String getItemsQuery(IRI itemNamespace) {
		log.debug("searching for members of " + itemNamespace);
		return "SELECT ?member\n"
				+ "WHERE {\n"
				+ "	\n"
				+ "	?collection rdf:first ?member .\n"
				+ "	FILTER (strstarts(str(?member), '"+itemNamespace.stringValue()+"'))\n"
				+ "}\n"
				+ "\n"
				+ "\n"
				+ "";
	}
}